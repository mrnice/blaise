#
# Toplevel Makefile 
#
MAKE = make
SNIPPEDDIR = snipped
PARSERDIR = ./parser
BLAISEDIR = ./blaise
AUTODOCDIR = ./autodoc
TESTSDIR = ./tests
OURCOMPILERDIRS = lib src
SUBDIRS = ./tools/pasdoc $(OURCOMPILERDIRS) lib/SystemUnitsExternal

.PHONY: subdirs $(SUBDIRS)

all: clean autodoc compile parser  fpcunit
#all: clean autodoc compile

compile: autodoc
	-for dir in $(SUBDIRS); do CC="fpc -g -Fu../lib" $(MAKE) -C $$dir all; done

rtl2dec:
	$(MAKE) -C $(PARSERDIR) --makefile=Makefile.src rtl2dec
	cd $(PARSERDIR) && ./rtl2dec

autodoc: pasdoc autodocdir
	-./tools/pasdoc/bin/pasdoc --write-uses-list --graphviz-classes --graphviz-uses $(shell find ./ -iname *.p -printf '%p ') -E $(AUTODOCDIR)

pasdoc:
	-$(MAKE) -C tools/pasdoc

parser: parsercopydir 
	cd $(PARSERDIR) && for i in $(OURCOMPILERDIRS); do CC="./parser" $(MAKE) --makefile Makefile.$$i all ; done

parsercopydir:
#	$(shell if [ ! -e $(PARSERDIR) ] then ; mkdir $(PARSERDIR) ; fi)
	-mkdir -p $(PARSERDIR)
	-for dir in $(OURCOMPILERDIRS); do cp $$dir/*.p $(PARSERDIR); \
	cp $$dir/*.dec $(PARSERDIR); \
	cp $$dir/parser $(PARSERDIR); \
	cp $$dir/Makefile $(PARSERDIR); \
	cp $$dir/rtl2dec $(PARSERDIR); \
	cp $$dir/dumpdec $(PARSERDIR); \
	mv $(PARSERDIR)/Makefile $(PARSERDIR)/Makefile.$$dir ;done

blaise: blaisecopydir
	cd $(BLAISEDIR) && for i in $(OURCOMPILERDIRS); do CC="./blaise" $(MAKE) --makefile Makefile.$$i all ; done

blaisecopydir:
	-mkdir -p $(BLAISEDIR)
	-for dir in $(OURCOMPILERDIRS); do cp $$dir/*.p $(BLAISEDIR); \
	cp $$dir/*.dec $(BLAISEDIR); \
	cp $(PARSERDIR)/blaise $(BLAISEDIR); \
	cp $(SNIPPEDDIR)/compile.sh $(BLAISEDIR); \
	cp $$dir/Makefile $(BLAISEDIR); \
	mv $(BLAISEDIR)/Makefile $(BLAISEDIR)/Makefile.$$dir ;done

autodocdir:
#	$(shell if [ ! -e $(AUTODOCDIR) ] then ; mkdir $(AUTODOCDIR) ; fi)
	-mkdir -p $(AUTODOCDIR)

fpcunit:
	cp -r $(PARSERDIR)/*.dec $(TESTSDIR)
	CC="fpc -g -Fu../lib" $(MAKE) -C $(TESTSDIR) all

.PHONY: clean
clean:
	for dir in $(SUBDIRS); do $(MAKE) -C $$dir $@; done
	$(MAKE) -C $(TESTSDIR) $@
	-rm -rf $(AUTODOCDIR) 
	-rm -rf $(PARSERDIR)
	-rm -rf $(BLAISEDIR)
