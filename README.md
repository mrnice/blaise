# Quick start

## Requirements

* 32 bit x86 cpu
* Linux >= 2.6.26
* make
* bash linked to /bin/sh
* FPC (2.2.4)

## How to compile

    make
    make blaise

## How to use

To be able to compile something in a convinced way you can simply use compile.sh which is located at ./blaise (after make blaise)

# Blaise

## A subset of Pascal Compiler

Written by:
* Bernhard Guillon, bernhard.guillon@begu.org
* Stefan Huber, stefan.huber2@sbg.ac.at

## Introduction

The goal of the course was to implement a self-compiling LL(1) compiler with the following features: typed source language (basic types, arrays, and records; no pointers), a concept for parametrized local hiding, and a concept for global hiding such as modules supporting separate compilation. The implementation was loosely based on Wirth’s textbook Compiler Construction. [Wir96]

## Specifications

The compiler is written in a subset of standard pascal (cf. EBNF). Our target language is GNU X86-assembly (as), a CISC architecture. For bootstrapping fpc was used. Assembling is performed by as. Linking is done with ld , object files are linked together into ELF(32 bit) binary files.

## Design - Overview

### Design of Blaise
![Design of Blaise](./images/design-of-blaise.svg "Design of Blaise")
### Generation of the object file
![Generation of the object file](./images/generation-of-object-file.svg "Generation of the object file")
### Generation of the ELF binary file
![Generation of the ELF binary file](./images/generation-of-elf-binary.svg "Generation of the ELF binary file")

### Functional description
Our self-compiling compiler consists of four parts: Scanner, Parser, Code Generation and Type-Checking. The Scanner reads a sample.p file character by character. Only a predefined set of characters is allowed. If a unknown character is read, an error message is written to standard output and also into a ".log"-file. The tokens extracted by the Scanner are passed to the Parser. The Parser is a top-down LL(1) parser. The Parser verifies the syntax of the file. If there are other files listed after the uses"-statement, the corresponding ".dec"-files are read. Those ".dec"-files consist of information about global variables, global types, procedures and functions. The information retrieved is inserted into the symbol table. And also a new "sample.dec" file is generated, which contains the information of the current file. First of all the Parser reads the declaration part of the file and inserts the extracted information into the symbol table. Next, memory requirement for the variables are defined in the "sample.s"-files. Afterwards the assembly code for the functions, procedures and the main program part is produced. Additionally we implemented a type-check for expressions and for statements, in which a type-check is needed. If a wrong type is used, an error message is generated. Then "sample.s" is assembled by as. To get an executable binary we link "ExternalSystemUnits.a", "sample.o" and "unit1.o,unit2.o, ..., unitn.o" together. The "ExternalSystemUnits.a" is a custom written assembly library. It’s main purpose is to provide low level routines for IO and memory handling.

## Scanner

The Scanner is similar to the scanner described in [Wir96]. The main procedure called getSym()returns a token, which consist of the string of the token and a number. The number is used as an identifier and the string for the error message. The string and the identifier of the token are written into a log file. If an error occurs, the current line, the current column and the unknown token are written to standard output.

## Parser

The Parser extracts the syntax of the file and if syntax errors are found, they are written to standard output. It tries to  ontinue parsing until the end of the file. This includes the generation of the ".log", ".dec" and ".s files.

## Code Generation

### Symbol Table and "dec" Files

Our approach for implementing the symbol table differs from [Wir96] in that it is based on a hash table rather than an array. The  eys of the hash table are strings and the payload is a record with the following elements:

* fileName: the name of the file, to which it belongs
* typeId: the type of the variable, constant or function and "notype" if procedure
* classId: class, e.g. variable, type, constant, etc.
* value: size of type, offset (record element) or value of number constant, else 0
* isVar: true if the variable is a pointer, else 0
* startArray: start value of a array, else 0
* endArray: end value of a array, else 0
* recordList: elements of a record or parameter of a procedure or function
* address: address of a variable, else -1

Constants, variables, types, functions and procedures are added into the hash table using the identifiers as keys. Only fields that are relevant for the respective symbol type are filled with the corresponding values. The remaining fields are filled with default values. The recordList is an array of integer. It has a fixed size of 12 elements in order to save memory. The values of the array are references to the entries of the elements of a record or the parameters of a procedure of a function. "-1" is used as a base value, indicating that there is no reference to an element or parameter. For example the payload of "var sampleVar:boolean" looks like:fileName: sample,typeId: boolean,classId: 2,value: 0,isVar: 0,startArray: 0,endArray: 0,recordList: -1, -1, ..., -1,address: 0. To implement different scopes we use different hash tables. There is a global hash table and each procedures or function has its local hash table. Beside the global hash table, units (an equivalent to modules) have their own hash table for the implementation part. To reduce memory consumption the number of hash tables is limited to six and the maximum size is 749 elements. The size is optimized to allow self-compilation. If another program or unit needs more elements or more hash tables, an error will occur. Most of the information, which is inserted to the global hash table, is also saved into ".dec"-files. These files are text files that are needed for separate compilation. They include information about global constants, variables, functions and procedures. An entry consists of the identifier and the payload. Almost all of ".dec"-files are generated by the compiler. One notable exception is ("rtl.dec"). This file contains information about the base types: ptrint, boolean, string, char, pointer, text, file, true and false. It has been written by hand.

### Type Check

Each Operation is type save. For example subtraction is only allowed with ptrint (=integer). We implemented a type check for assignment statements, conditional statements and repetitive statements. Even pointers are type save, such that you can’t assign the address of an integer to a pointer to a string. There is one exception. If there is the need to assign the address of a char pointer to an integer pointer, you can use the type "pointer". This type can be used in the following way. For example we have the given variables:var myPointer:pointer; pInt:^ ptrint; pChar:^ char. You can assign the address of the char pointer to the integer pointer with the following assignment statements:pointer := pInt; pChar := pointer;.

### Instruction Set

We use only a small subset of as instructions which are:
* movl src, dest- move src to dest
* xchgl sd1, sd2- exchange sd1 and sd
* addl src, dest- addition
* subl src, dest- subtraction
* incl dest- increase dest by one
* cmpl src, dest- neither src or dest changes but may change flags
* negl dest- change sign or two’s complement
* imul src- signed multiplication
* idiv src- signed division
* andl src, dest- and
* orl src, dest- or
* testl src, dest- an AND that does not change dest, only flags
* je, jne, jmp- jump equal, not equal, jump
* sarl count, dest- shift dest count bits to the right
* int- interrupt
* ret- return from subroutine
* pushl- push long to stack
* leal- get pointer of e.g. -12(%ebx)

### Memory Management

#### Global Memory

Heap memory is allocated with the.data assembler directive, followed by a global label. Single variables are allocated with the.long directive for 4 bytes size variables. For data structures of variable size the.skip directive is used. Example: Say we’d like to allocate memory for a string variable. This could be done with a statement like.skip 256, 0. That way we end up with allocating 256 bytes of zero initialized memory. Array access works by adding the start address to the address of the global label. To get the offset of a specific array element, the size of the type is multiplied with the array index. Access to record elements works in a similar way. To get the address we add to the base address of the record an offset, which indicates the position of the element in the record. When compiling units there is a second label for variables with a unit-wide scope, those can’t be accessed by other units or programs. Another 256 bytes are allocated on the heap for string concatenation purposes. Strings are implemented with a fixed maximum size of 256 bytes, with the first byte being a length counter, just as in regular pascal.

#### Local Memory

Local variables are stored on the stack. Parameters are stored +8 relative to the base pointer and variables declared within the procedures are stored in the negative direction relative to the base pointer (e.g. -4 could be the address of an integer).

### String Concatentation and Comparison

The ’+’ operator for string concatenation is implemented as a loop in assembly code. The size and the characters of the first string are moved to the global memory space for string concatenation. Then the characters of the second string are added after the last character of the first string and the size of the second string is added to the first byte. String Comparison is also implemented by generating a loop in assembly code. The strings are compared byte by byte. As soon as two differing bytes are found, then the loop terminates and inequality is returned. Otherwise the loop terminates after the value of the count register equals the size of the strings and equality is returned.

### Procedure and Function Calls

First of all we compare the procedure with the prototype utilizing the symbol table. Afterwards we start to generate code. Of course expressions within parameters of the procedure are allowed as long as the type matches. To save the current register state all registers are pushed to the stack. Next step is to push the references or the values to the stack. After the procedure returns the saved registers will be removed from the stack. The difference between functions and procedures in blaise and pascal is that the return value is stored in a variable which is located in the scope in which the function was declarated. Pascal functions lack the ability to return before end is reached.

### Parameter passing

To be able to use the parameters of the environment we copy and convert argument one into a global string.

### Optimization

We only implemented constant folding.

## Library

To be able to use Free Pascal compiler(fpc) as bootstrap compiler we decided to write wrapper procedures for each internal fpc function that we use.

### ExternalSystemUnits

The ExternalSystemUnits contains standard I/O and memory procedures and are written in assembly. Since these procedures utilize Linux system calls, we no longer depend on external libraries such as the libc.

### SystemUnits

More sophisticated FPC procedures (such as transforming a string containing numbers into an integer) were written in blaise, thereby harnessing the power of a high level language.

## Separate Compilation

### Overview

Pascal supports separate compilation with the concept of programs and units. A program is the main file which includes units it utilizes with a uses statement. Units may also include other units with uses. Units are not able to include programs. Units have their own scope additional to the global scope. This scope is only accessible within the unit and has its own symbol table. Apart from this detail, symbols from units are handled completely the same way as their global scope counterparts.

### Implementation Details

Pascal demands that every unit has a begin block which initializes all the variables of its unit. There has to be an end block, as well. Because as expects only a single start: label, the solution was to provide functions for all modules begin and end blocks and all those functions at the start of every program/unit. Our make based build system then takes care about the right calls of blaise, as, ar and ld.

## Features

* assignment statements: ie.myStringArr[i]:= anotherString + ’hello’;
* composite statements: sequence, conditional statements (if-statement), repetitive statements (while, repeat until and for)
* procedures with parameter: call by value/reference
* modules (units in Pascal)
* basic data types: ptrint(=integer), boolean, char, string
* composite types: array, records
* functions
* pointers
* Linux syscalls (I/O and malloc)

## Build System

### Overview

The build system consists of:
* make - GNU build system
* bash - a free UNIX shell
* pasdoc - a tool to generate documentation out of pascal source code
* fpc - Free Pascal Compiler (our bootstrap compiler)
* parser- our compiler, compiled from our sources with fpc
* blaise- our compiler, compiled from our source withparser
* blaise2- our compiler, finally compiled withblaise
* as- GNU assembler
* ld- GNU linker
* ar- tool for archives. used to create ExternalSystemUnits

The build system directories:

* lib - I/O, hash table, system procedures, wrapper
* src - scanner, parser, code generation, type check
* tests - unit tests for libs and essential units
* autodoc - auto generated documentation
* parser - auto generated frommake: contains complete compiler source, parser and blaise
* blaise - auto generated frommake: blaise contains complete compiler source, blaise and blaise

### Build Flow / Bootstrapping

#### Prerequisites

The source tree needs to be clean, so all leftover files and directories from previous builds are removed. A cross reference documentation of the source code is generated with pasdoc.

#### Building the first (bootstrap) compiler

![Building the first (bootstrap) compiler](./images/building-bootstrap-compiler.svg "Building the first (bootstrap) compiler")

Compiling the the sources in src/ lib/ with fpc leads to a binary called parser. Despite it’s name it is already a fully functional compiler, the name has been kept for historical reasons only.

#### Building the blaise compiler

![Building the blaise compiler](./images/building-the-blaise-compiler.svg "Building the blaise compiler")

All sources from src/ and lib/ are copied into a new directory called "parser/". The parser binary generated above is also copied into this directory and will be used to compile all sources, which are the same as for parser itself. After this step every source (".p") file will be compiled into a assembly (".s") file, and a declaration (".dec") file. Assembly files are assembled into object files (".o") with as. Object files are linked together into blaise, an ELF 32 bit executable binary. This demonstrates how blaise can be used to compile itself.

#### The role of ExternalSystemUnits in bootstrapping

ExternalSystemUnits.pacts as a wrapper for basic IO and memory management routines. It will be compiled along with the other source code, but only the resulting "ExternalSystemUnits.dec" is of interest. The (generated) assembly file "ExternalSystemUnits.s" will be discarded. It’s functionality is implemented in several hand written assembly files which reside below lib/SystemUnitsExternal/. Those files will be assembled with as and the resulting object files will be combined into theExternalSystemUnits.a library with ar. This library will be used for linking our executables, thereby acting as a replacement for the regular fpc runtime library.

#### Unit tests

We use fpc to compile the unit tests, which are invoked from the build system later on.

#### Fix point test / blaise2 compiler

Calling make blaise will perform a fix point test. All sources from parser/src/ parser/lib/ are copied into a new directory called
"blaise/". The blaise binary generated above is also copied into this directory. Now exactly the same procedure as with building blaise itself is performed. Instead of parser, this time we use blaise for compiling itself. The resulting binary is called blaise2. Now something like diff can be used to verify that the blaise and blaise2 binary are indeed identical.

## EBNF
```
program = program_heading block ".".
program_heading = program identifier ";" [uses_list].
uses_list = uses identifier {"," identifier} ";".
block = declaration_part statement_part.
declaration_part = [constant_definition_part] [type_definition_part]
[var_definition_part] [proc_func_declaration_part].
constant_definition_part = const constant_definition ";"
{constant_definition ";"}.
constant_definition = identifier "=" constant.
type_definition_part = type type_definition ";" {type_definition ";"}.
type_definition = identifier "=" type | array_type | record_type |
file_type | pointer_type.
var_definition_part = var var_definition ";" {var_definition ";"}.
var_definition = identifier {"," identifier} ":" type.
proc_func_declaration_part =
{ (proc_declaration | func_declaration) ";" }.
proc_declaration = procedure_heading ";" block.
procedure_heading = procedure identifier [formal_parameter_list].
func_declaration = function_heading ";" block.
function_heading = function identifier [formal_parameter_list] ":"
type.
formal_parameter_list = "(" formal_parameter_section
{ ";" formal_parameter_section } ")".
formal_parameter_section = value_parameter_section |
variable_parameter_section.
value_parameter_section = identifier_list ":" type.
variable_parameter_section = var identifier_list ":" type.
statement_part = begin [statement_sequence] end.
statement_sequence = statement {";" statement}.
statement = assign_proc_statement | repetitive_statement |
conditional_statement.
assign_proc_statement = {"^"} identifier (assignment_statement |
procedure_statement ).
assignment_statement = {("." {"^"} identifier ) |
("[" expression "]")} ":=" expression.
expression_list = expression { "," expression }.
procedure_statement = [("(" expression_list ")") |
("." identifier "(" expression_list ")" )].
repetitive_statement = while_statement | for_statement |
repeat_statement.
while_statement = while expression do statement_part.
for_statement = for identifier ":=" expression (to | downto)
expression do statement_part.
repeat_statement = repeat statement_part until expression.
conditional_statement = if_statement | case_statement.
if_statement = if expression then (statement_part | statement)
[ else (statement_part | statement) ].
expression = simple_expression
[ relational_operator simple_expression ].
simple_expression = [sign] term { addition_operator term }.
relational_operator = "=" | "<>" | "<" | "<=" | ">" | ">=".
sign = "+" | "-".
term = factor { multiplication_operator factor }.
factor = variable | number | string | specialchar |
"(" expression ")" | not factor.
variable = {"^"} identifier {("." {"^"} identifier ) |
("[" expression "]")}.
multiplication_operator = "*" | div | mod | and.
addition_operator = "+" | "-" | or.
number = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9".
string = "’" character { character } "’".
constant = ([sign] number) | string | specialchar.
identifier = letter {letter | number}.
letter = "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" |
"J" | "K" | "L" | "M" | "N" | "O" | "P" | "Q" | "R" | "S" | "T" |
"U" | "V" | "W" | "X" | "Y" | "Z" | "a" | "b" | "c" | "d" | "e" |
"f" | "g" | "h" | "i" | "j" | "k" | "l" | "m" | "n" | "o" | "p" |
"q" | "r" | "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z".
type = simple_type | file_type | (identifier [ "[" number "]" ]).
simple_type = identifier.
identifier_list = identifier { "," identifier }.
array_type = array "[" number .. number | identifier "]" of type.
file_type = file of type.
record_type = record field_list end.
pointer_type = ^ type.
field_list = [ fixed_part [ ";" ] ].
fixed_part = record_section { ";" record_section }.
record_section = identifier { "," identifier } ":" type.
unit = unit_heading interface_part implementation_part [block] ".".
unit_heading = unit identifier ";".
interface_part = interface [uses_list] [declaration_part]
[proc_func_interface_declarations].
proc_func_interface_declarations =
{procedure_heading | function_heading}.
implementation_part = implementation [constant_definition_part]
[var_definition_part] proc_func_declaration_part.
```
## References

[Wir96] Niklaus Wirth.Compiler construction. Addison Wesley Longman Publishing Co., Inc., Redwood City, CA, USA, 1996.