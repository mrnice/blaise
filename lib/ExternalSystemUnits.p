{External Functions like writeln str assign which will be replaced with assembler code only}
unit ExternalSystemUnits;

interface

const
	maxRecordListEntries = 12;

type
{ used for writing into .dec-file}
  strList = array [0 .. maxRecordListEntries] of string;
  intRecArr = array [0 .. maxRecordListEntries] of ptrint;
  declarationRecord = record
	identifier : string;
	fileName : string;
	typeId : string;
	classId : ptrint;
	value : ptrint; //size, offset, constant number
    	isVar : boolean;
	startArray : ptrint;
	endArray : ptrint;
	recordList : strList;
	address : ptrint;
  end;
  decFile = file of declarationRecord;


{ wrapepr for halt() procedure }
{ wrapper procedures for which we allready have assembler code}
procedure println(s:string);
procedure print(var s:string);

procedure closeFile(var f:file);
procedure closeTextFile(var f:text);
procedure eHalt(exitcode: ptrint );
procedure eErase(var fake:string;var f:file);
procedure getOrd(var c:char;var ergebnis:ptrint);
procedure getChr(i:ptrint;var ergebnis:char);
procedure EndOfTextfile(var f:text;var ergebnis:boolean);
procedure EndOfDeclarationRecordfile(var f:decFile;var ergebnis:boolean);
procedure getParamStr(i:ptrint;var ergebnis:string);
procedure openTextFileForRead(var s:string;var f:text);
procedure openTextFileForWrite(var s:string;var f:text);
procedure openDecFileForRead(var s:string;var f:decFile);
procedure openDecFileForWrite(var s:string;var f:decFile);
procedure readTextFileChar(var f:text;var c:char);
procedure writeTextFile(var f:text;s:string);
procedure writelnTextFile(var f:text;s:string);
procedure writeDecFile(var f:decFile;size:ptrint;var r:declarationRecord);
procedure readDecFile(var f:decFile;size:ptrint;var r:declarationRecord);
procedure sysBrk(i:ptrint;var p:pointer);
implementation

{ wrapepr for halt() procedure }
procedure eHalt(exitcode: ptrint );
begin
  halt(exitcode);
end;

procedure eErase(var fake:string;var f:file);
begin
  erase(f);
end;

procedure println(s:string);
begin
  writeln(s);
end;

procedure print(var s:string);
begin
  write(s);
end;

procedure writeTextFile(var f:text;s:string);
begin
  write(f,s);
end;


procedure writeDecFile(var f:decFile;size:ptrint;var r:declarationRecord);
begin
  write(f,r);
end;

procedure readDecFile(var f:decFile;size:ptrint;var r:declarationRecord);
begin
  read(f,r);
end;

procedure resetDeclarationRecord(var f:decFile);
begin
  reset(f);
end;

procedure resetTextFile(var f:text);
begin
  reset(f);
end;

procedure readTextFileChar(var f:text;var c:char);
begin
  read(f,c);
end;

procedure EndOfTextfile(var f:text;var ergebnis:boolean);
begin
  ergebnis:=eof(f);
end;

procedure EndOfDeclarationRecordfile(var f:decFile;var ergebnis:boolean);
begin
  ergebnis:=eof(f);
end;

procedure writelnTextFile(var f:text;s:string);
begin
  writeln(f,s);
end;

procedure getOrd(var c:char;var ergebnis:ptrint);
begin
  ergebnis:=ord(c);
end;

procedure getChr(i:ptrint;var ergebnis:char);
begin
  ergebnis:=chr(i);
end;

procedure getParamStr(i:ptrint;var ergebnis:string);
begin
  if (i = 1) then
  ergebnis:=ParamStr(1);
end;


procedure closeFile(var f:file);
begin
  close(f);
end;

procedure closeTextFile(var f:text);
begin
  close(f);
end;

procedure openTextFileForRead(var s:string;var f:text);
begin
	assign(f,s);
	reset(f);
end;
procedure openTextFileForWrite(var s:string;var f:text);
begin
	assign(f,s);
	rewrite(f);
end;
procedure openDecFileForRead(var s:string;var f:decFile);
begin
	assign(f,s);
	reset(f);
end;
procedure openDecFileForWrite(var s:string;var f:decFile);
begin
	assign(f,s);
	rewrite(f);
end;

//NON Pascal extention
procedure sysBrk(i:ptrint;var p:pointer);
begin
end;

end.
