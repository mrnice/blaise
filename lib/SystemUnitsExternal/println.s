# ExternalSystemUnits.println.s - an print procedure
.section .text
.type ExternalSystemUnits.println, @function
.globl ExternalSystemUnits.println
ExternalSystemUnits.println:
  pushl %ebp
  movl %esp, %ebp
//preserve 260 byte for local vars on the stack
  sub  $260,%esp
//-260(%ebp) = saveEdi
//-256(%ebp) = convertedString

//save edi
  movl %edi,-260(%ebp)

//clear i
	movl $0, %edi
//	get string pointer
	leal 8(%ebp),%ecx
//clear edx
	movl $0, %edx 
//get string length (load first byte to lower edx)
	movb (%ecx), %dl
//set i to string length
	add %edx,%edi 

//Copy \n to the string
//get pointer to local var
	leal -256(%ebp),%edx
//add edi to it
	add %edi,%edx
	//add $1,%edx
	movb $10, (%edx)

//clear eax
movl $0,%eax

	je end_string
string_loop:
	leal 8(%ebp),%edx
//add index to the pointer
	add %edi,%edx
//copy one byte to ah
	movb (%edx),%ah
//substract edi
	sub $1,%edi
//get pointer to local var
	leal -256(%ebp),%edx
//add edi to it
	add %edi,%edx
// move one byte to the given memory
	movb %ah, (%edx)
	cmp $0,%edi
	jg string_loop
end_string:


//restore edi
//  movl saveEDI,%edi
  movl -260(%ebp),%edi
//put sys write to eax
        movl    $4,%eax
        movl    $2,%ebx

//clear edx
	movl $0, %edx 
//get string length (load first byte to lower edx)
	movb (%ecx), %dl

//add 1 to string length
	add  $1,%edx
	//movl $20,%edx

//load address to ecx
        leal    -256(%ebp),%ecx
//trap syscall
        int     $0x80

//return
  movl %ebp, %esp
  popl %ebp
  ret



