# ExternalSystemUnits.readDecFile.s - an print procedure
.section .text
.type print, @function
.globl ExternalSystemUnits.readDecFile
ExternalSystemUnits.readDecFile:
  pushl %ebp
  movl %esp, %ebp
//preserve 260 byte for local vars on the stack
  sub  $4,%esp
//-4(%ebp) = saveEdi
//-256(%ebp) = convertedString

//save edi
  movl %edi,-4(%ebp)

//get pointer to filedescriptor
	movl	8(%ebp),%edi
	xorl	%ebx,%ebx

//put sys write to eax
        movl    $3,%eax
	movl	(%edi),%ebx
//get length
	movl 12(%ebp), %edx
//load address to ecx
        movl    16(%ebp),%ecx
//trap syscall
        int     $0x80

//restore edi
//  movl saveEDI,%edi
  movl -4(%ebp),%edi

//return
  movl %ebp, %esp
  popl %ebp
  ret



