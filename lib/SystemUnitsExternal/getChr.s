# ExternalSystemUnits.getChr.s - an open syscall function
.section .text
.type ExternalSystemUnits.getChr, @function
.globl ExternalSystemUnits.getChr
ExternalSystemUnits.getChr:
  pushl %ebp
  movl %esp, %ebp
//move first argument into eax
  movl 8(%ebp), %eax
//save referenced data
//move result to the second arg 
movl 12(%ebp),%ebx
movb %al,(%ebx)

//return
  movl %ebp, %esp
  popl %ebp
  ret
