# ExternalSystemUnits.EndOfTextfile.s - an print procedure
.section .text
.type ExternalSystemUnits.EndOfTextfile, @function
.globl ExternalSystemUnits.EndOfTextfile
ExternalSystemUnits.EndOfTextfile:
  pushl %ebp
  movl %esp, %ebp
//preserve 230 byte for local vars on the stack
  sub  $8,%esp
//-8(%ebp) = saveEdi
//-4(%ebp) = buffer

//save edi
  movl %edi,-8(%ebp)

//try to get one byte after our current position

//get pointer to filedescriptor
	movl	8(%ebp),%edi
	xorl	%ebx,%ebx

//read one byte
//put sys read to eax
	movl	$3,%eax
	movl	(%edi),%ebx
        leal    -4(%ebp),%ecx
	movl	$1,%edx
//trap syscall
        int     $0x80

//move return value of read to the given reference
	cmp $1,%eax
	jge set_false
	movl 12(%ebp),%ebx
	movl $1,(%ebx)
	jmp cont_
set_false:
	movl 12(%ebp),%ebx
	movl $0,(%ebx)
cont_:

//get pointer to filedescriptor
	movl	8(%ebp),%edi
	xorl	%ebx,%ebx

//set current position -1
//put sys lseek to eax
        movl    $19,%eax
	movl	(%edi),%ebx
//use current value as base
	movl	$-1,%ecx 
//add 1 to current value
	movl	$1,%edx 
//trap syscall
        int     $0x80

//restore edi
//  movl saveEDI,%edi
  movl -8(%ebp),%edi

//return
  movl %ebp, %esp
  popl %ebp
  ret



