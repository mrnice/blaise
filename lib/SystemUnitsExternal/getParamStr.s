# ExternalSystemUnits.getParamStr.s - an open syscall function
.section .text
.type ExternalSystemUnits.getParamStr, @function
.globl ExternalSystemUnits.getParamStr
ExternalSystemUnits.getParamStr:
  pushl %ebp
  movl %esp, %ebp
//move global pointer into edx
  movl $global_paramstr_1,%edx

//copy pointer to pascal string into memory
  movl 12(%ebp),%ebx
//copy length of pascal string into ecx
  xorl %ecx,%ecx
  movb (%edx),%cl

  xorl %edi,%edi

loop_copy_string:
//loop until we find a \0
	xorl	%eax,%eax
	movb	(%edx),%al
	movb	%al,(%ebx)
	cmp 	%ecx,%edi
	je	end_loop_copy_string
	inc	%edi
	inc	%ebx
	inc	%edx
	jmp	loop_copy_string
end_loop_copy_string:


//for debug only
// print argument
   //movl ,%ecx
//   pushl 12(%ebp)
//   call ExternalSystemUnits.print
//clear stack
//   addl $4, %esp

//return
  movl %ebp, %esp
  popl %ebp
  ret
