# ExternalSystemUnits.eHalt.s - an exit syscall function
.section .text
.type ExternalSystemUnits.eHalt, @function
.globl ExternalSystemUnits.eHalt
ExternalSystemUnits.eHalt:
  pushl %ebp
  movl %esp, %ebp
//move first argument into ebx
  movl 8(%ebp), %ebx
//set eax to sys exit
  movl $1, %eax
//trap (do a syscall)
  int $0x80

//return
  movl %ebp, %esp
  popl %ebp
  ret
