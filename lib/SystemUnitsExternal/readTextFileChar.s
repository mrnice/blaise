# ExternalSystemUnits.readTextFileChar.s - an open syscall function
.section .text
.type ExternalSystemUnits.readTextFileChar, @function
.globl ExternalSystemUnits.readTextFileChar
ExternalSystemUnits.readTextFileChar:
  pushl %ebp
  movl %esp, %ebp
//preserve 230 byte for local vars on the stack
  sub  $8,%esp
//-8(%ebp) = saveEdi
//-4(%ebp) = buffer

//save edi
  movl %edi,-8(%ebp)

//try to get one byte after our current position

//get pointer to filedescriptor
	movl	8(%ebp),%edi
	xorl	%ebx,%ebx
//read one byte
//put sys read to eax
	movl	$3,%eax
	movl	(%edi),%ebx
        movl    12(%ebp),%ecx
	movl	$1,%edx
//trap syscall
        int     $0x80

//restore edi
//  movl saveEDI,%edi
  movl -8(%ebp),%edi

//return
  movl %ebp, %esp
  popl %ebp
  ret
