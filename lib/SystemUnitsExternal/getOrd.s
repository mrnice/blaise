# ExternalSystemUnits.getOrd.s - an open syscall function
.section .text
.type ExternalSystemUnits.getOrd, @function
.globl ExternalSystemUnits.getOrd
ExternalSystemUnits.getOrd:
  pushl %ebp
  movl %esp, %ebp
//move first argument into eax
  xorl %eax,%eax
  //leal 8(%ebp),%ecx
  //movb (%ecx), %al
  movl 8(%ebp),%ecx
  movb (%ecx), %al
//save referenced data
//move result to the second arg 
//leal 12(%ebp),%ebx
movl 12(%ebp),%ebx
movl %eax,(%ebx)

//return
  movl %ebp, %esp
  popl %ebp
  ret
