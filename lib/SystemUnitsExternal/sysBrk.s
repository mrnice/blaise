# ExternalSystemUnits.sysBrk.s - an brk syscall function
.section .text
.type ExternalSystemUnits.sysBrk, @function
.globl ExternalSystemUnits.sysBrk
ExternalSystemUnits.sysBrk:
  pushl %ebp
  movl %esp, %ebp

//get current position
        movl    $45,%eax
	movl	$0,%ebx
	int	$0x80

//extend memory with the given size
	//save eax
	movl	%eax,%ecx
        movl    8(%ebp),%ebx
	add	%eax,%ebx

	movl	$45,%eax
//trap syscall
        int     $0x80

//calculate pointer
	subl	8(%ebp),%eax
	cmp	%eax,%ecx
	je	pointer_ok
//else
	movl	12(%ebp),%ecx
	movl	$-1,(%ecx)
	jmp	return_from_brk
pointer_ok:
//save referenced data
//move second argument (pointer) into ecx and afterwards 
//move the result of the syscall to the memory location of it)
movl 12(%ebp),%ecx
movl %eax,(%ecx)

//return
return_from_brk:
  movl %ebp, %esp
  popl %ebp
  ret
