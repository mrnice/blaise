# ExternalSystemUnits.openTextFileForRead.s - an open syscall function
.section .text
.type ExternalSystemUnits.openTextFileForRead, @function
.globl ExternalSystemUnits.openTextFileForRead
ExternalSystemUnits.openTextFileForRead:
  pushl %ebp
  movl %esp, %ebp
//preserve 260 byte for local vars on the stack
  sub  $260,%esp
//-260(%ebp) = saveEdi
//-256(%ebp) = convertedString

//save edi
  movl %edi,-260(%ebp)

//clear i
	movl $0, %edi
//	get string pointer
	movl 8(%ebp),%ecx
//clear edx
	movl $0, %edx 
//get string length (load first byte to lower edx)
	movb (%ecx), %dl
//set i to string length
	add %edx,%edi 

//Copy \0 to the string
//get pointer to local var
	leal -256(%ebp),%edx
//add edi to it
	add %edi,%edx
	movb $0, (%edx)
//clear eax
movl $0,%eax

	je end_string
string_loop:
	movl 8(%ebp),%edx
//add index to the pointer
	add %edi,%edx
//copy one byte to ah
	movb (%edx),%ah
//substract edi
	sub $1,%edi
//get pointer to local var
	leal -256(%ebp),%edx
//add edi to it
	add %edi,%edx
// move one byte to the given memory
	movb %ah, (%edx)
	cmp $0,%edi
	jg string_loop
end_string:

//restore edi
//  movl saveEDI,%edi
  movl -260(%ebp),%edi

//put sys open to eax
        movl    $5,%eax
//load address to ebx
        leal    -256(%ebp),%ebx
	movl $0000, %ecx
//trap syscall
        int     $0x80
//save referenced data
//move second argument (pointer) into ecx and afterwards 
//move the result of the syscall to the memory location of it)
movl 12(%ebp),%ecx
movl %eax,(%ecx)

//return
  movl %ebp, %esp
  popl %ebp
  ret



