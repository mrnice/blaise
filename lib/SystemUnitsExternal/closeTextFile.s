# ExternalSystemUnits.closeTextFile.s - an close syscall function (same as closeFile)
.section .text
.type ExternalSystemUnits.closeTextFile, @function
.globl ExternalSystemUnits.closeTextFile
ExternalSystemUnits.closeTextFile:
  pushl %ebp
  movl %esp, %ebp
//move first argument into ebx
  movl 8(%ebp), %ebx
//set eax to sys close
  movl $6, %eax
//trap (do a syscall)
  int $0x80

//return
  movl %ebp, %esp
  popl %ebp
  ret
