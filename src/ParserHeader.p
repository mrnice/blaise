{variable definition and error function}
unit ParserHeader;

interface
uses ExternalSystemUnits, SystemUnits, Scanner,ObjFNVHashTable;

const
	{different Classes}
	CLASS_NOCLASS = -1;
	CLASS_CONST = 0; 
	CLASS_TYPE = 1; 
	CLASS_VAR = 2; 
	CLASS_FILE = 3; 
	CLASS_RECORDVAR = 4; 
	CLASS_PROCEDURE = 5; 
	CLASS_FUNCTION = 6;
	CLASS_PARAMETER = 7;
	CLASS_UNIT = 8;

	DECL_NODECL = -1;
	DECL_CONST = 1;
	DECL_TYPE = 2;
	DECL_VAR = 3;
	DECL_PROC_FUNC = 4;
	{max Uses List}
	maxUsesList = 20;
	strSizeInt = 256;

var
  declarationsfile : text; // used to write into a .dec file
  declarationsFilename:string; //used to fake pascal i/o
  usesfile : text; // used to read from a .dec file
  log : string[255]; // used to create the logfile
  declaration : declarationRecord; // used to write into a .dec file
  usesList : array[0..maxUsesList] of string; // list in which all units are listed, which have been declared
  token : symbol; // holds currently scanned symbol
  globalHashTable, implementationHashTable, currentLocalHashTable:ptrint;
  currentProcIdentifier,strSize : string;
  usesPart, isProgram : boolean;

procedure printFileNameAndCurrentLine();
{ for standard error output while parsing }
procedure error(var identifier: string; expected:string);
{ error if duplacte entry in hash-table }
procedure errorHash(identifier:string);
{ array overflow in identifier list currently max 100}
procedure errorIdentifierList();
{ error output, if an identifier, while type check was not found}
procedure errorType(identifier:string);
{ false types are to be added, compared, etc. }
procedure errorFalseType(typeId1:string; typeId2:string; op:string);
{ in array declaration only constant ptrints are allowed }
procedure errorArrayDeclarationFalseType(var typeId : string);
{ error output, if a sign is used, where it must not be used}
procedure errorSign();
{ error output, if identifier is not a const }
procedure errorConst(identifier:string);
{ error output, if bad declaration sequence }
procedure errorBadDeclarationSequence();
{ error output, if array index out of bounds }
procedure errorOutofBounds();
{ error output, if parentheses are used within string concatenation}
procedure errorStrParenthesis();
{ information that identifer has not been implemented }
procedure notImplemented(identifier : string);
{ while using the stack an error occured }
procedure errorStack();
{ used @ in the wrong place }
procedure errorAt();
{ closes all files and then terminates the parser }
procedure errorPointer();
//procedure terminate();
{ returns a string containing the name of a class identifier }
procedure getClassName(classId : ptrint; var name : string);
{ set the file name }
procedure setFileName(name : string);
{ get the file name }
procedure getFileName(var name : string);
{ set declaration part }
procedure setDeclPart(declPart : ptrint);
{ get declaration part }
procedure getDeclPart(var declPart : ptrint);

implementation

var
  fileName : string; // holds the name of the file parsed
  declarationPart : ptrint;

procedure printFileNameAndCurrentLine();
var
	paramString :string;
	lineInfStr : string;
begin
  SystemUnits.str(line,conv); SystemUnits.str(co,conv2);
  ExternalSystemUnits.getParamStr(1,paramString);
  lineInfStr := paramString + '(' + conv + ',' + conv2 + ')';
  ExternalSystemUnits.print(lineInfStr);	
end;

{ for standard error output while parsing}
procedure error(var identifier: string; expected:string);
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Error: ' + expected + ' expected but "' + identifier + '" found');
  //ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: ' + expected + ' expected but "' + identifier + '" found');
end;

{ error if duplacte entry in hash-table }
procedure errorHash(identifier:string);
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Error: Duplicate identifier "' + identifier + '"');
  //ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: Duplicate identifier "' + identifier + '"');
end;

{ array overflow in identifier list currently max 100}
procedure errorIdentifierList();
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Error: too many identifiers');
  //ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: too many identifiers');
end;

{ false types are to be added, compared, etc. }
procedure errorType(identifier:string);
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Error: Identifier not found "' + identifier + '"');
  //ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: Identifier not found "' + identifier + '"');
end;

{ false types are to be added, compared, etc. }
procedure errorFalseType(typeId1:string; typeId2:string; op:string);
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Error: Operation "' + op + '" not supported for types "'+ typeId1 + '" and "' + typeId2 + '"');
  //ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: Operation "' + op + '" not supported for types "'+ typeId1 + '" and "' + typeId2 + '"');
end;

{ in array declaration only constant ptrints are allowed }
procedure errorArrayDeclarationFalseType(var typeId : string);
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Error: "'+ typeId + '" found, but needed ptrint');
  //ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: "'+ typeId + '" found, but needed ptrint');
end;

{ error output, if a sign is used, where it must not be used}
procedure errorSign();
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Error: sign not allowed');
  //ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: sign not allowed');
end;

{ error output, if identifier is not a const}
procedure errorConst(identifier:string);
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Error: ' + identifier + ' isn' + #39 + 't const');
  //ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: ' + identifier + ' isn' + #39 + 't const');
end;

{ error output, if bad declaration sequence }
procedure errorBadDeclarationSequence();
begin
	printFileNameAndCurrentLine();
	ExternalSystemUnits.println(' bad declaration sequence');
	//ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' bad declaration sequence');
end;

{ error output, if array index out of bounds }
procedure errorOutofBounds();
begin
	printFileNameAndCurrentLine();
	ExternalSystemUnits.println(' Error: array index out of bounds');
	//ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: array index out of bounds');	
end;

{ error output, if parentheses are used within string concatenation}
procedure errorStrParenthesis();
begin
	printFileNameAndCurrentLine();
	ExternalSystemUnits.println(' Error: parentheses are not allowed within string concatenation');		
	//ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Error: parentheses are not allowed within string concatenation');	
end;

{ information that identifer has not been implemented }
procedure notImplemented(identifier : string);
begin
	printFileNameAndCurrentLine();
	ExternalSystemUnits.println(identifier + ' are not implemented');
	//ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ') ' + identifier + ' are not implemented');
end;

{ while using the stack an error occured }
procedure errorStack();
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Stack error');
  //ExternalSystemUnits.println(ParamStr(1) + '(' + conv + ',' + conv2 + ')' + ' Stack error');
end;

{ used @ in the wrong place }
procedure errorAt();
begin
  printFileNameAndCurrentLine();
  ExternalSystemUnits.println(' Used "@" at the wrong place');	
end;

procedure errorPointer();
begin
	printFileNameAndCurrentLine();
	ExternalSystemUnits.println(' Error: ^ is not allowed more than one times');
end;

{ closes all files and then terminates the parser }
{procedure terminate();
begin
  ExternalSystemUnits.println('Fatal: Compilation aborted');
  // delete .dec file
  ExternalSystemUnits.closeFile(declarationsfile);
  declarationsFilename:=log + '.dec';
  ExternalSystemUnits.openDecFileForRead(declarationsFilename,declarationsfile);
  //ExternalSystemUnits.assignFile(declarationsfile,declarationsFilename);
  //ExternalSystemUnits.assignFile(declarationsfile, log + '.dec');
  ExternalSystemUnits.eErase(declarationsFilename,declarationsfile);
  
  // close all files before termination
  ExternalSystemUnits.closeTextFile(textfile);
  ExternalSystemUnits.closeTextFile(logfile);
  
  // program termination
  ExternalSystemUnits.eHalt(1);
end;}

{ returns a string containing the name of a class identifier }
procedure getClassName(classId : ptrint; var name : string);
begin
	if (classId = CLASS_NOCLASS) then name := 'noclass';
	if (classId = CLASS_CONST) then name := 'const';
	if (classId = CLASS_TYPE) then name := 'type';
	if (classId = CLASS_VAR) then name := 'variable';
	if (classId = CLASS_FILE) then name := 'file';
	if (classId = CLASS_RECORDVAR) then name := 'recordvar';
	if (classId = CLASS_PROCEDURE) then name := 'procedure';
	if (classId = CLASS_FUNCTION) then name := 'function';
	if (classId = CLASS_PARAMETER) then name := 'parameter';
	if (classId = CLASS_UNIT) then name := 'unit';
end;

{ set the file name }
procedure setFileName(name : string);
begin
	fileName := name;
end;

{ get the file name }
procedure getFileName(var name : string);
begin
	name := fileName;
end;

{ set declaration part }
procedure setDeclPart(declPart : ptrint);
begin
	declarationPart := declPart;
end;

{ get declaration part }
procedure getDeclPart(var declPart : ptrint);
begin
	declPart := declarationPart;
end;

end.
