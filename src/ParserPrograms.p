{ parsing programs }
unit ParserPrograms;

interface
uses ExternalSystemUnits, SystemUnits, IOProcedures, Scanner, ParserHeader, ParserDeclarations,ObjFNVHashTable, AddressCalculation;

{Program parsing part}

{ program = program_heading block ".".}
procedure parseProgram();
{ program_heading = program identifier ";" [uses_list].}
procedure parseProgramHeading();
{ uses_list = uses identifier «"," identifier» ";".}
procedure parseUses();
{ used to read from .dec file, specified at uses, to write the information in the the hashtable}
procedure readDecFile(var filename :string;var scope :string);

implementation

{////////////////////////////////////////////////////////////////////////////////////////////////////////////////////}
{Program parsing part}
{////////////////////////////////////////////////////////////////////////////////////////////////////////////////////}

{ program = program_heading block ".".}
procedure parseProgram();
begin
  isProgram := true;
  // start with global positive addressing mode
  AddressCalculation.setAddressCalcMode(POSITIVE);
  // initialize currentAddress
  AddressCalculation.setStartingAddress(0);
  AddressCalculation.writeAssemblyFileHeaderPrograms(); // first of all write header information
  parseProgramHeading();
  ParserDeclarations.parseBlockPrograms();
  Scanner.getSym(token);
  if not (token.sym = point) then ParserHeader.error(token.id,'point');
end;

{ program_heading = program identifier ";" [uses_list].}
procedure parseProgramHeading();
var
	fileName,scope :string;
begin
  if not (token.sym = KEYprog) then ParserHeader.error(token.id,'program');
  Scanner.getSym(token);
  if not (token.sym = ident) then ParserHeader.error(token.id,'identifier');
  Scanner.getSym(token);
  if not (token.sym = semicolon) then 
    ParserHeader.error(token.id,'";"')
  else
    Scanner.getSym(token);

  // first of all read rtl.dec
  fileName := 'rtl';
  scope := 'global';
  readDecFile(fileName,scope);  
  
  {uses is optional}
  if (token.sym = KEYuses) then 
  begin 
    parseUses();
    Scanner.getSym(token);
  end;
  
 {declarationsfile initialization after uses}
  declarationsFilename:=log + '.dec';
  ExternalSystemUnits.openTextFileForWrite(declarationsFilename,declarationsfile);
  //ExternalSystemUnits.assignFile(declarationsfile,declarationsFilename);
  //ExternalSystemUnits.rewriteDeclarationRecordFile(declarationsfile);
  //Rewrite (declarationsfile);
end;
  
{ uses_list = uses identifier «"," identifier» ";".}
procedure parseUses();
var
  helpPayload : hashTablePayload;
  i : ptrint;
begin
  if not (token.sym = KEYuses) then ParserHeader.error(token.id,'uses');
  Scanner.getSym(token);

  if not (token.sym = ident) then 
	ParserHeader.error(token.id,'identifier')
  else
  begin
	// insert first element in useslist
	usesList[0] := token.id;
	usesList[1] := '#end';
	helpPayload.typeId := 'notype';
	helpPayload.classId := CLASS_UNIT;
	ParserDeclarations.addIntoHashTableDecFiles(token.id,helpPayload);
	//Writeln(token.id + ' ' + helpPayload.typeId + ' ' + helpPayload.classId);
	readDecFile(token.id,token.id);
  end;

  Scanner.getSym(token);
    
  if (token.sym = comma) then
  {additional identifiers}
  begin
    i := 1;
    repeat
    begin
      if not (token.sym = comma) then 
        ParserHeader.error(token.id,'","')
      else
        Scanner.getSym(token);
      
        if not (token.sym = ident) then 
		ParserHeader.error(token.id,'identifier')
	else
	begin
		usesList[i] := token.id;
		helpPayload.typeId := 'notype';
		helpPayload.classId := CLASS_UNIT;
		ParserDeclarations.addIntoHashTableDecFiles(token.id,helpPayload);
		//Writeln(token.id + ' ' + helpPayload.typeId + ' ' + helpPayload.classId);
		readDecFile(token.id,token.id);
		i := i + 1;
	end;
	
	Scanner.getSym(token);
    end;
    until (token.sym = semicolon);
    usesList[i] := '#end';
  end;
end;

procedure checkIfRecordList(var identifier : string; var isPartOfRecList : boolean; var recProcIdentifier : string);
var
	i, strLength : ptrint;
	ch : char;
begin
	ExternalSystemUnits.getOrd(identifier[0],strLength);
	isPartOfRecList := false;
	i := 1;
	while (i <= strLength) and (identifier[i] <> '>') do
	begin
		recProcIdentifier[i] := identifier[i];
		i := i + 1;
	end;
	if (identifier[i] = '>') then 
		isPartOfRecList := true;
	ExternalSystemUnits.getChr(i-1,ch);
	recProcIdentifier[0] := ch;
end;

{ used to read from .dec file, specified at uses, to write the information in the the hashtable}
procedure readDecFile(var filename :string;var scope :string);
var
  helpPayload: hashTablePayload;
  EndOfFile, isPartOfRecList : boolean;
  identifier, recProcIdentifier, tmpStr : string;
  declarationsFilename:string;
  count, payloadPointer, tmpInt : ptrint;
  elementArr : intRecArr;
begin
	declarationsFilename:=filename + '.dec';
	ExternalSystemUnits.openTextFileForRead(declarationsFilename,usesfile);
	ExternalSystemUnits.EndOfTextfile(usesfile,EndOfFile);
	isPartOfRecList := false;
	count := 0;
	while not EndOfFile do
	begin
		IOProcedures.readDecTextFile(usesfile, identifier, helpPayload);
		if ((helpPayload.classId = CLASS_PROCEDURE) or (helpPayload.classId = CLASS_FUNCTION)) and (filename <> 'rtl') then 
			identifier := filename + '.' + identifier;
		if not isPartOfRecList then 
		begin
			checkIfRecordList(identifier,isPartOfRecList,recProcIdentifier);
			recProcIdentifier := filename + '.' + recProcIdentifier;
		end
		else
		begin
			if (recProcIdentifier = identifier) then
			begin
				//SystemUnits.str(count, tmpStr);
				//ExternalSystemUnits.println(recProcIdentifier + ' ' + tmpStr);
				helpPayload.recordList := elementArr;
				isPartOfRecList := false;
				count := 0;
			end;
		end;
		ParserDeclarations.addIntoHashTableDecFiles(identifier,helpPayload);
		if isPartOfRecList then
		begin
			if (recProcIdentifier <> identifier) then
			begin
				ObjFNVHashTable.getPayloadPointer(currentLocalHashTable,identifier,payloadPointer);
				//SystemUnits.str(count, tmpStr);
				//ExternalSystemUnits.println(identifier + ' ' + tmpStr);
				elementArr[count] := payloadPointer;
				count := count + 1;
			end;
		end;
		ExternalSystemUnits.EndOfTextfile(usesfile,EndOfFile);
	end;
	ExternalSystemUnits.closeTextFile(usesfile);

	{ExternalSystemUnits.openDecFileForRead(declarationsFilename,usesfile);
	//ExternalSystemUnits.assignFile(usesfile, filename + '.dec');
	//ExternalSystemUnits.resetDeclarationRecord(usesfile);
	//Reset(usesfile);
  
	// read from file
	ExternalSystemUnits.EndOfDeclarationRecordfile(usesfile,EndOfFile);
	//while not eof(usesfile) do
	while not EndOfFile do
	begin
		//FIXME:USE HASHTABLE TO SEARCH FOR THE SIZE
		ExternalSystemUnits.readDecFile(usesfile,4120,declaration);
		//Read(usesfile,declaration);
		if ((declaration.classId = CLASS_PROCEDURE) or (declaration.classId = CLASS_FUNCTION)) and (filename <> 'rtl') then 
		begin
			identifier := filename + '.' + declaration.identifier;
		end
		else
			identifier := declaration.identifier;
		helpPayload.fileName:=filename;
		helpPayload.typeId:=declaration.typeId;
		helpPayload.classId:=declaration.classId;
		helpPayload.value:=declaration.value;
		helpPayload.isVar:=declaration.isVar;
		helpPayload.startArray:=declaration.startArray;
		helpPayload.endArray:=declaration.endArray;
		helpPayload.recordList:=declaration.recordList;
		helpPayload.address:=declaration.address;
		ParserDeclarations.addIntoHashTableDecFiles(identifier,helpPayload);

		ExternalSystemUnits.EndOfDeclarationRecordfile(usesfile,EndOfFile);
	end;}
end;	
	
end.
