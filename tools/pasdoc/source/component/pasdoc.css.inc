{ -*- buffer-read-only: t -*- }
{ DON'T EDIT -- this file was automatically generated from "pasdoc.css" }
'body { font-family: Verdana,Arial; ' + LineEnding + 
'  color: black; background-color: white; ' + LineEnding + 
'  font-size: 12px; }' + LineEnding + 
'body.navigationframe { font-family: Verdana,Arial; ' + LineEnding + 
'  color: white; background-color: #787878; ' + LineEnding + 
'  font-size: 12px; }' + LineEnding + 
'' + LineEnding + 
'img { border:0px; }' + LineEnding + 
'' + LineEnding + 
'a:link {color:#C91E0C; text-decoration: none; }' + LineEnding + 
'a:visited {color:#7E5C31; text-decoration: none; }' + LineEnding + 
'a:hover {text-decoration: underline; }' + LineEnding + 
'a:active {text-decoration: underline; }' + LineEnding + 
'' + LineEnding + 
'a.navigation:link { color: white; text-decoration: none; font-size: 12px;}' + LineEnding + 
'a.navigation:visited { color: white; text-decoration: none; font-size: 12px;}' + LineEnding + 
'a.navigation:hover { color: white; font-weight: bold; ' + LineEnding + 
'  text-decoration: none; font-size: 12px; }' + LineEnding + 
'a.navigation:active { color: white; text-decoration: none; font-size: 12px;}' + LineEnding + 
'' + LineEnding + 
'a.bold:link {color:#C91E0C; text-decoration: none; font-weight:bold; }' + LineEnding + 
'a.bold:visited {color:#7E5C31; text-decoration: none; font-weight:bold; }' + LineEnding + 
'a.bold:hover {text-decoration: underline; font-weight:bold; }' + LineEnding + 
'a.bold:active {text-decoration: underline; font-weight:bold; }' + LineEnding + 
'' + LineEnding + 
'a.section {color: green; text-decoration: none; font-weight: bold; }' + LineEnding + 
'a.section:hover {color: green; text-decoration: underline; font-weight: bold; }' + LineEnding + 
'' + LineEnding + 
'ul.useslist a:link {color:#C91E0C; text-decoration: none; font-weight:bold; }' + LineEnding + 
'ul.useslist a:visited {color:#7E5C31; text-decoration: none; font-weight:bold; }' + LineEnding + 
'ul.useslist a:hover {text-decoration: underline; font-weight:bold; }' + LineEnding + 
'ul.useslist a:active {text-decoration: underline; font-weight:bold; }' + LineEnding + 
'' + LineEnding + 
'ul.hierarchy { list-style-type:none; }' + LineEnding + 
'ul.hierarchylevel { list-style-type:none; }' + LineEnding + 
'' + LineEnding + 
'p.unitlink a:link {color:#C91E0C; text-decoration: none; font-weight:bold; }' + LineEnding + 
'p.unitlink a:visited {color:#7E5C31; text-decoration: none; font-weight:bold; }' + LineEnding + 
'p.unitlink a:hover {text-decoration: underline; font-weight:bold; }' + LineEnding + 
'p.unitlink a:active {text-decoration: underline; font-weight:bold; }' + LineEnding + 
'' + LineEnding + 
'tr.list { background: #FFBF44; }' + LineEnding + 
'tr.list2 { background: #FFC982; }' + LineEnding + 
'tr.listheader { background: #C91E0C; color: white; }' + LineEnding + 
'' + LineEnding + 
'table.wide_list { border-spacing:2px; width:100%; }' + LineEnding + 
'table.wide_list td { vertical-align:top; padding:4px; }' + LineEnding + 
'' + LineEnding + 
'table.markerlegend { width:auto; }' + LineEnding + 
'table.markerlegend td.legendmarker { text-align:center; }' + LineEnding + 
'' + LineEnding + 
'table.sections { background:white; }' + LineEnding + 
'table.sections td {background:lightgray; }' + LineEnding + 
'' + LineEnding + 
'table.summary td.itemcode { width:100%; }' + LineEnding + 
'table.detail td.itemcode { width:100%; }' + LineEnding + 
'' + LineEnding + 
'td.itemname {white-space:nowrap; }' + LineEnding + 
'td.itemunit {white-space:nowrap; }' + LineEnding + 
'td.itemdesc { width:100%; }' + LineEnding + 
'' + LineEnding + 
'div.nodescription { color:red; }' + LineEnding + 
'dl.parameters dt { color:blue; }' + LineEnding + 
'' + LineEnding + 
'/* Various browsers have various default styles for <h6>,' + LineEnding + 
'   sometimes ugly for our purposes, so it''s best to set things' + LineEnding + 
'   like font-size and font-weight in out pasdoc.css explicitly. */' + LineEnding + 
'h6.description_section { ' + LineEnding + 
'  /* font-size 100% means that it has the same font size as the ' + LineEnding + 
'     parent element, i.e. normal description text */' + LineEnding + 
'  font-size: 100%;' + LineEnding + 
'  font-weight: bold; ' + LineEnding + 
'  /* By default browsers usually have some large margin-bottom and ' + LineEnding + 
'     margin-top for <h1-6> tags. In our case, margin-bottom is' + LineEnding + 
'     unnecessary, we want to visually show that description_section' + LineEnding + 
'     is closely related to content below. In this situation' + LineEnding + 
'     (where the font size is just as a normal text), smaller bottom' + LineEnding + 
'     margin seems to look good. */' + LineEnding + 
'  margin-bottom: 0em;' + LineEnding + 
'}' + LineEnding + 
'' + LineEnding + 
'/* Style applied to Pascal code in documentation ' + LineEnding + 
'   (e.g. produced by @longcode tag) } */' + LineEnding + 
'span.pascal_string { color: #000080; }' + LineEnding + 
'span.pascal_keyword { font-weight: bolder; }' + LineEnding + 
'span.pascal_comment { color: #000080; font-style: italic; }' + LineEnding + 
'span.pascal_compiler_comment { color: #008000; }' + LineEnding + 
'span.pascal_numeric { }' + LineEnding + 
'span.pascal_hex { }' + LineEnding + 
'' + LineEnding + 
'p.hint_directive { color: red; }' + LineEnding + 
'' + LineEnding + 
'input#search_text { }' + LineEnding + 
'input#search_submit_button { }' + LineEnding + 
'' + LineEnding + 
'acronym.mispelling { background-color: #ffa; }' + LineEnding + 
'' + LineEnding + 
'/* Actually this reduces vertical space between *every* paragraph' + LineEnding + 
'   inside list with @itemSpacing(compact). ' + LineEnding + 
'   While we would like to reduce this space only for the' + LineEnding + 
'   top of 1st and bottom of last paragraph within each list item.' + LineEnding + 
'   But, well, user probably will not do any paragraph breaks' + LineEnding + 
'   within a list with @itemSpacing(compact) anyway, so it''s' + LineEnding + 
'   acceptable solution. */' + LineEnding + 
'ul.compact_spacing p { margin-top: 0em; margin-bottom: 0em; }' + LineEnding + 
'ol.compact_spacing p { margin-top: 0em; margin-bottom: 0em; }' + LineEnding + 
'dl.compact_spacing p { margin-top: 0em; margin-bottom: 0em; }' + LineEnding + 
'' + LineEnding + 
'/* Style for table created by @table tags:' + LineEnding + 
'   just some thin border.' + LineEnding + 
'   ' + LineEnding + 
'   This way we have some borders around the cells' + LineEnding + 
'   (so cells are visibly separated), but the border ' + LineEnding + 
'   "blends with the background" so it doesn''t look too ugly.' + LineEnding + 
'   Hopefully it looks satisfactory in most cases and for most' + LineEnding + 
'   people. ' + LineEnding + 
'   ' + LineEnding + 
'   We add padding for cells, otherwise they look too close.' + LineEnding + 
'   This is normal thing to do when border-collapse is set to' + LineEnding + 
'   collapse (because this eliminates spacing between cells). ' + LineEnding + 
'*/' + LineEnding + 
'table.table_tag { border-collapse: collapse; }' + LineEnding + 
'table.table_tag td { border: 1pt solid gray; padding: 0.3em; }' + LineEnding + 
'table.table_tag th { border: 1pt solid gray; padding: 0.3em; }' + LineEnding + 
'' + LineEnding + 
'table.detail {' + LineEnding + 
'  border: 1pt solid gray;' + LineEnding + 
'  margin-top: 0.3em;' + LineEnding + 
'  margin-bottom: 0.3em;' + LineEnding + 
'}' + LineEnding + 
''
