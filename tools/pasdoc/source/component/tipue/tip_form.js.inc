{ -*- buffer-read-only: t -*- }
{ DON'T EDIT -- this file was automatically generated from "tip_form.js" }
'// Tipue 1.63 (modified for pasdoc)' + LineEnding + 
'' + LineEnding + 
'' + LineEnding + 
'// ---------- script properties ----------' + LineEnding + 
'' + LineEnding + 
'' + LineEnding + 
'var results_location = "_tipue_results.html";' + LineEnding + 
'' + LineEnding + 
'' + LineEnding + 
'// ---------- end of script properties ----------' + LineEnding + 
'' + LineEnding + 
'' + LineEnding + 
'function search_form(tip_Form) {' + LineEnding + 
'	if (tip_Form.d.value.length > 0) {' + LineEnding + 
'		document.cookie = ''tid='' + escape(tip_Form.d.value) + ''; path=/'';' + LineEnding + 
'		document.cookie = ''tin=0; path=/'';' + LineEnding + 
'		parent.content.location.href = results_location;' + LineEnding + 
'	}' + LineEnding + 
'}' + LineEnding + 
''
