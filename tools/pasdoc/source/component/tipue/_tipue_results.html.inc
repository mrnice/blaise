{ -*- buffer-read-only: t -*- }
{ DON'T EDIT -- this file was automatically generated from "_tipue_results.html" }
'<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">' + LineEnding + 
'' + LineEnding + 
'<html>' + LineEnding + 
'<head>' + LineEnding + 
'<title>Search Results</title>' + LineEnding + 
'' + LineEnding + 
'<link rel="StyleSheet" type="text/css" href="pasdoc.css">' + LineEnding + 
'' + LineEnding + 
'<!-- The following lines are required and must be in the <head> section of your HTML document -->' + LineEnding + 
'' + LineEnding + 
'<script language="JavaScript1.3" type="text/javascript" src="tip_data.js"></script>' + LineEnding + 
'<script language="JavaScript1.3" type="text/javascript" src="tip_search.js"></script>' + LineEnding + 
'' + LineEnding + 
'<!-- End of required <head> section -->' + LineEnding + 
'' + LineEnding + 
'###-PASDOC-INSERT-HEAD-###' + LineEnding + 
'' + LineEnding + 
'</head>' + LineEnding + 
'<body>' + LineEnding + 
'' + LineEnding + 
'<div>' + LineEnding + 
'' + LineEnding + 
'<!-- The following JavaScript uses the tip_Num variable to return the number of matches found -->' + LineEnding + 
'' + LineEnding + 
'<script language="JavaScript1.3" type="text/javascript">' + LineEnding + 
'if (tip_Num == 0) {' + LineEnding + 
'  document.write(''No matches found'');' + LineEnding + 
'}' + LineEnding + 
'if (tip_Num == 1) {' + LineEnding + 
'  document.write(''1 match found'');' + LineEnding + 
'}' + LineEnding + 
'if (tip_Num > 1) {' + LineEnding + 
'  document.write(tip_Num, '' matches found'');' + LineEnding + 
'}' + LineEnding + 
'</script>' + LineEnding + 
'' + LineEnding + 
'<hr>' + LineEnding + 
'' + LineEnding + 
'<!-- The following line returns the results -->' + LineEnding + 
'' + LineEnding + 
'<script language="JavaScript1.3" type="text/javascript">tip_out()</script>' + LineEnding + 
'' + LineEnding + 
'<hr>' + LineEnding + 
'' + LineEnding + 
'<p>' + LineEnding + 
'<a href="http://www.tipue.com" target="_parent"><img src="tipue_b1.png" title="Tipue" alt="Tipue" border=0 width=138 height=32></a>' + LineEnding + 
'</div>' + LineEnding + 
'' + LineEnding + 
'</body>' + LineEnding + 
'</html>' + LineEnding + 
''
