{ This translation is from
  https://sourceforge.net/tracker/?func=detail&atid=354213&aid=1767925&group_id=4213
  Pasted into separate file, to minimize chance of messing character codes.
}

procedure TPasDocLanguages.SetLanguageChinese_gb2312;
begin
  FTranslation[trAuthor] := '作者';
  FTranslation[trAuthors] := '作者';
  FTranslation[trAutomated] := 'Automated';
  FTranslation[trCio] := '类型';
  FTranslation[trClass] := '类';
  FTranslation[trClasses] := '类';
  FTranslation[trClassHierarchy] := '类继承';
  FTranslation[trConstants] := '常数';
  FTranslation[trCreated] := '新建时间';
  FTranslation[trDeclaration] := '声明';
  FTranslation[trDescription] := '描述';
  FTranslation[trParameters] := '参数';
  FTranslation[trReturns] := '返回';
  FTranslation[trExceptions] := '异常';
  FTranslation[trExceptionsRaised] := '被抛出异常';
  FTranslation[trEnum] := '枚举';
  FTranslation[trDispInterface] := 'DispInterface';
  FTranslation[trFields] := 'Fields';
  FTranslation[trFunctionsAndProcedures] := '函数';
  FTranslation[trHelp] := '帮助';
  FTranslation[trHierarchy] := '继承';
  FTranslation[trIdentifiers] := '标志符';
  FTranslation[trInterface] := '接口';
  FTranslation[trLegend] := '图标';
  FTranslation[trMarker] := '标记';
  FTranslation[trVisibility] := '可见性';
  FTranslation[trMethods] := '方法';
  FTranslation[trLastModified] := '最近修改';
  FTranslation[trName] := '名字';
  FTranslation[trNone] := 'None';
  FTranslation[trObject] := '对象';
  FTranslation[trObjects] := '对象';
  FTranslation[trOverview] := '概要';
  FTranslation[trPrivate] := 'Private';
  FTranslation[trStrictPrivate] := 'Strict Private';
  FTranslation[trProperties] := 'Properties';
  FTranslation[trProtected] := 'Protected';
  FTranslation[trStrictProtected] := 'Strict Protected';
  FTranslation[trPublic] := 'Public';
  FTranslation[trImplicit] := 'Implicit';
  FTranslation[trPublished] := 'Published';
  FTranslation[trType] := '类型';
  FTranslation[trTypes] := '类型';
  FTranslation[trUnit] := '单元';
  FTranslation[trUnits] := '单元';
  FTranslation[trVariables] := '变量';
  FTranslation[trGvUses] := '单元依赖关系';
  FTranslation[trGvClasses] := '类继承图';
  FTranslation[trHeadlineCio] := '全部类型';
  FTranslation[trHeadlineConstants] := '全部常数';
  FTranslation[trHeadlineFunctionsAndProcedures] := '全部函数';
  FTranslation[trHeadlineIdentifiers] := '全部标志符';
  FTranslation[trHeadlineTypes] := '全部类型';
  FTranslation[trHeadlineUnits] := '全部单元';
  FTranslation[trHeadlineVariables] := '全部变量';
  FTranslation[trSummaryCio] := '类型提要';
  FTranslation[trWarningOverwrite] :=
    'Warning: Do not edit - this file has been created automatically and is likely be overwritten';
  FTranslation[trWarning] := '警告';
  FTranslation[trGeneratedBy] := 'Generated by';
  FTranslation[trOnDateTime] := 'on';
  FTranslation[trDeprecated] := '这个符号已经过期';
  FTranslation[trPlatformSpecific] := '这个符号仅仅用于特定平台';
  FTranslation[trLibrarySpecific] := '这个符号仅仅用于特性的库';
  FTranslation[trIntroduction] := '前言';
  FTranslation[trConclusion] := '后记';
  FTranslation[trSearch] := '搜索';
  FTranslation[trSeeAlso] := '参见';
  FTranslation[trValues] := '值';
  FTranslation[trNoCIOs] := '这个没有包括任何类,对象，接口';
  FTranslation[trNoCIOsForHierarchy] := '这个没有包括任何类,对象，接口';
  FTranslation[trNoTypes] := '这个单元没有包括任何类型';
  FTranslation[trNoVariables] := '这个单元没有任何变量';
  FTranslation[trNoConstants] := '这个单元没有任何常数';
  FTranslation[trNoFunctions] := '这个单元没有任何函数贺过程';
  FTranslation[trNoIdentifiers] := '这个单元没有任何标志符';
  FTranslation[trProgram] := '程序';
  FTranslation[trLibrary] := '库';
end;

