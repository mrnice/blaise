#!/bin/bash
COMPILER=./blaise
PROGNAME=$(echo $1 | sed 's/\(.*\).p/\1/' )
HELLO="welcome to blaise"
echo $HELLO
# loop over all args
for ARG ; do
        FILE=$(echo ${ARG} | sed 's/\(.*\).p/\1/' )
	if [ -f ${ARG} ]
	then
		echo NOTE: $COMPILER ${ARG}
		$COMPILER ${ARG}
		echo NOTE: as -g --32 -o $FILE.o $FILE.s
		as -g --32 -o $FILE.o $FILE.s
		COMPSTRING=$COMPSTRING" "$FILE.o
	else
		echo "Error file not found ${ARG}"
		exit 1
	fi
done
echo NOTE: ld -melf_i386 -o $PROGNAME $COMPSTRING ParserUnits.a ParserLibs.a  ../lib/SystemUnitsExternal/ExternalSystemUnits.a
ld -melf_i386 -o $PROGNAME $COMPSTRING ParserUnits.a ParserLibs.a  ../lib/SystemUnitsExternal/ExternalSystemUnits.a
